# Web Scraper Example

Example webscraper written in python, used for helping students with their work

## Specification:

We would need faculty data scraped of below listed emails. We are after emails and full names of professors, lecturers, students within biomedical, health, medicine sciences.

Please let me know if any of your students can help with this, how much time they would need and how much would it costs us. The data would need to be supplied in an excel sheet with following fields - First Name, Last Name, Email Address. Thank you.

- [ ] https://www.tcd.ie/   
- [ ] https://www.dit.ie/ 
- [ ] https://www.ait.ie/ 
- [ ] https://www.cit.ie
- [ ] https://www.ncirl.ie 
- [ ] https://www.rcsi.com   
- [ ] https://www.gmit.ie 
- [ ] https://www.gmit.ie 
- [ ] https://www.lit.ie 
- [ ] https://www.griffith.ie 
- [ ] https://www.it-tallaght.ie 
- [ ] https://www.dbs.ie
- [ ] https://www.itsligo.ie 
- [ ] https://www.itcarlow.ie 
- [ ] https://www.lyit.ie 
- [ ] https://www.mic.ul.ie 
- [ ] https://www.tcd.ie 
- [ ] https://www.rcpi.ie 
- [ ] https://www.iadt.ie 
- [ ] https://www.itb.ie 
- [ ] https://tudublin.ie 
- [ ] https://corkcollegeofcommerce.ie 
- [ ] https://www.dkit.ie 
- [ ] https://www.lcfe.ie 
- [ ] https://www.hiberniacollege.com/‎ 
- [ ] https://www.libertiescollege.ie 
- [ ] https://www.sligocfe.ie 
- [ ] https://www.dorset-college.ie

# Installation

Install dependencies:
```shell script
python3 -m venv venv3
source venv3/bin/activate
pip install -r requirements.txt
```

# Running and development

A nice an fast way to develop and debug is to use ipython

```shell script
ipython -i start/my_first_webscrape.py
```

This completes the webscrape and launches a terminal when the end of script occurs