from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

my_url = 'https://www.tcd.ie/genetics-microbiology/people/'

uClient = uReq(my_url)
page_html = uClient.read()
uClient.close()

page_soup = soup(page_html, "html.parser")

filename = "tcd.ie.School_of_Genetic_and_Microbiology.csv"
f = open(filename, "w")
headers = "full_name", "email\n"

f.write("headers")


# Fetch parent container
containers = page_soup.findAll("div", {"class": "tcdpeople-person"})

# Itterate through parent containers, a single parent container contains

'''
<div class="tcdpeople-person">
<div class="tcdpeople-col-1">
<a class="tcdpeople-person-profilelink" href="ubond/"><span>Dr. Ursula Bond</span></a>
</div>
<div class="tcdpeople-col-2">
<span class="tcdpeople-person-jobtitle">Associate Professor, Microbiology</span>
</div>
<div class="tcdpeople-col-3">
<a class="tcdpeople-person-email" href="mailto:ubond@tcd.ie"><span>ubond@tcd.ie</span></a>
</div>
<div class="tcdpeople-col-4">
<a class="tcdpeople-person-phone" href="tel:0035318962578"><span>2578</span></a>
</div>
</div>

'''

for container in containers:

	full_name = container.find(class_="tcdpeople-person-profilelink").text
	email = container.find(class_="tcdpeople-person-email").text

	f.write(full_name + "," + email + "\n")

	print(full_name + " " + email)


f.close()

